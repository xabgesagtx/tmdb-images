package com.canceledsystems.tmdbimages

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(properties = ["s3.key=key","s3.secret=secret","s3.bucket=bucket","s3.region=region","s3.endpoint=https://example.com","tmdb.apiKey=apiKey"])
class TmdbImagesApplicationTests {

	@Test
	fun contextLoads() {
	}

}
