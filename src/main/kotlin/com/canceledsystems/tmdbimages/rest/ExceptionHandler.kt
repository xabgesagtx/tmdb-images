package com.canceledsystems.tmdbimages.rest

import com.canceledsystems.tmdbimages.service.S3Exception
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandler {

    @ExceptionHandler(S3Exception::class)
    fun s3Exception(e: S3Exception): ResponseEntity<String> {
        return ResponseEntity(e.message, HttpStatus.BAD_GATEWAY)
    }
}