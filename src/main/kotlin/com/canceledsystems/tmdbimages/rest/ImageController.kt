package com.canceledsystems.tmdbimages.rest

import com.canceledsystems.tmdbimages.service.Image
import com.canceledsystems.tmdbimages.service.ImageProxy
import com.canceledsystems.tmdbimages.service.ImageSize
import com.canceledsystems.tmdbimages.service.ImageType
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("image")
@CrossOrigin("*")
class ImageController(private val imageProxy: ImageProxy) {

    @GetMapping
    fun image(@RequestParam("path") path: String, @RequestParam("type") type: ImageType, @RequestParam("size") size: ImageSize): ResponseEntity<ByteArray> {
        return imageProxy.getImage(path, size, type)?.let(this::createResponse) ?: create404response()
    }

    private fun createResponse(image: Image): ResponseEntity<ByteArray> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.parseMediaType(image.mimeType)
        headers.cacheControl = "max-age=37739520, public"
        return ResponseEntity(image.data, headers, HttpStatus.OK)
    }

    private fun create404response(): ResponseEntity<ByteArray> {
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }
}