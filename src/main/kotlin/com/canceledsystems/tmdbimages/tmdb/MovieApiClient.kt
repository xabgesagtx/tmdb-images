package com.canceledsystems.tmdbimages.tmdb

import com.github.xabgesagtx.tmdb.TmdbApi
import com.github.xabgesagtx.tmdb.configuration.GetApiConfigurationResponse
import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component

@Component
class MovieApiClient(private val tmdbApi: TmdbApi) {

    private val log = LoggerFactory.getLogger(javaClass)

    @Cacheable("tmdbConfiguration")
    fun getConfiguration(): GetApiConfigurationResponse {
        log.info("Loading tmdb configuration")
        return tmdbApi.configuration().apiConfiguration
    }

}