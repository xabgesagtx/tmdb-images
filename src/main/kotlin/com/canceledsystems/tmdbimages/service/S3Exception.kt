package com.canceledsystems.tmdbimages.service

class S3Exception(message: String, cause: Throwable): RuntimeException(message, cause)