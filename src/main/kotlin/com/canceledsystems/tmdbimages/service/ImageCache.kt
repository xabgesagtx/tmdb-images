package com.canceledsystems.tmdbimages.service

import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class ImageCache(private val repository: ImageRepository) {

    private val log = LoggerFactory.getLogger(ImageCache::class.java)

    fun getFromCache(type: ImageType?, width: String, path: String): Image? {
        var result: Image? = null
        if (type != null && StringUtils.isNotBlank(width) && StringUtils.isNotBlank(path)) {
            val imagePath = "$type/$width/${path.removePrefix("/")}"
            result = repository.get(imagePath)
        } else {
            log.info("Some parameters aren't set properly. Cannot get image from cache")
        }
        return result
    }

    fun saveToCache(type: ImageType?, width: String, path: String, image: Image) {
        if (type != null && StringUtils.isNotBlank(width) && StringUtils.isNotBlank(path)) {
            val imagePath = "$type/$width/${path.removePrefix("/")}"
            repository.save(imagePath, image)
        } else {
            log.info("Some parameters aren't set properly. Cannot store image to cache.")
        }
    }

}