package com.canceledsystems.tmdbimages.service

enum class ImageType {
    POSTER, BACKDROP, STILL, PROFILE, LOGO
}
