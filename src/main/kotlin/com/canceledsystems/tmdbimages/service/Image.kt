package com.canceledsystems.tmdbimages.service

data class Image(val mimeType: String,
                 val data: ByteArray)