package com.canceledsystems.tmdbimages.service

import com.canceledsystems.tmdbimages.config.TmdbApiProperties
import com.canceledsystems.tmdbimages.service.ImageType.*
import com.canceledsystems.tmdbimages.tmdb.MovieApiClient
import com.github.xabgesagtx.tmdb.configuration.GetApiConfigurationResponse
import org.slf4j.LoggerFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestClientException
import org.springframework.web.util.UriComponentsBuilder
import java.util.stream.Collectors
import java.util.stream.Stream

@Service
class ImageProxy(private val cache: ImageCache, private val apiClient: MovieApiClient, private val apiProperties: TmdbApiProperties, restTemplateBuilder: RestTemplateBuilder) {

    private val log = LoggerFactory.getLogger(ImageProxy::class.java)

    private val restTemplate = restTemplateBuilder.build()

    fun getImage(path: String, size: ImageSize, type: ImageType): Image? {
        val configuration = apiClient.getConfiguration().images
        val secureBaseUrl = configuration.secureBaseUrl
        val sizeString = getSizeString(configuration, type, size)
        var result = cache.getFromCache(type, sizeString, path)
        if (result == null) {
            val uri = UriComponentsBuilder.fromUriString(secureBaseUrl).path(sizeString).path(path).queryParam("api_key", apiProperties.apiKey).build().toUri()
            var imageEntity: ResponseEntity<Image>? = null
            try {
                imageEntity = restTemplate.getForEntity(uri, Image::class.java)
            } catch (e: RestClientException) {
                log.warn("Failed to get image {} due to error: {}", uri, e.message)
            }

            if (imageEntity != null) {
                val resultImage = imageEntity.body
                if (resultImage != null) {
                    result = resultImage
                    cache.saveToCache(type, sizeString, path, resultImage)
                }
            }
        }
        return result
    }

    private fun getSizeString(configuration: GetApiConfigurationResponse.Image, type: ImageType, size: ImageSize): String {
        val sizeStrings = getSizeStrings(type, configuration)
        return getSizeStringForImageSize(size, sizeStrings)
    }

    private fun getSizeStrings(type: ImageType, configuration: GetApiConfigurationResponse.Image): List<String> {
        return when (type) {
            POSTER -> configuration.posterSizes
            LOGO -> configuration.logoSizes
            BACKDROP -> configuration.backdropSizes
            PROFILE -> configuration.profileSizes
            STILL -> configuration.stillSizes
        }
    }

    private fun getSizeStringForImageSize(size: ImageSize, sizeStrings: List<String>): String {
        val result: String
        val widths = sizeStrings.stream()
                .filter { it.startsWith("w") }
                .map { it.removePrefix("w") }
                .flatMap { x ->
                    var innerResult = Stream.empty<Int>()
                    try {
                        innerResult = Stream.of(Integer.parseInt(x))
                    } catch (e: NumberFormatException) {
                        log.info("Not a number size string found: \"{}\"", x)
                    }

                    innerResult
                }
                .filter { x ->
                    val minResult = size.minWidth?.let {x >= size.minWidth} ?: true
                    val maxResult = size.maxWidth?.let {x <= size.maxWidth} ?: true
                    minResult && maxResult
                }
                .sorted()
                .collect(Collectors.toList())
        if (widths.size == 1) {
            result = "w" + widths[0]
        } else if (widths.size > 1) {
            var index = 0
            if (size.withMaxWidth() && size.withMinWidth()) {
                index = widths.size / 2
            } else if (size.withMinWidth()) {
                index = widths.size - 1
            }
            result = "w" + widths[index]
        } else if (sizeStrings.contains("original")) {
            result = "original"
        } else {
            result = getSizeStringForImageSize(ImageSize.ANY, sizeStrings)
        }
        return result
    }
}