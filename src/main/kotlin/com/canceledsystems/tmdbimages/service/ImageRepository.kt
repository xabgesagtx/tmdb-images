package com.canceledsystems.tmdbimages.service

interface ImageRepository {

    fun get(path: String): Image?

    fun save(path: String, image: Image)
}