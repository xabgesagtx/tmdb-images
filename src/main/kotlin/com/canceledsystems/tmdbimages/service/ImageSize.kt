package com.canceledsystems.tmdbimages.service

enum class ImageSize(val minWidth: Int?, val maxWidth: Int?) {
    EXTRASMALL(null, 200), SMALL(200, 500), MEDIUM(401, 600), LARGE(601, null), ANY(null, null);

    fun withMinWidth(): Boolean {
        return minWidth != null
    }

    fun withMaxWidth(): Boolean {
        return maxWidth != null
    }
}