package com.canceledsystems.tmdbimages.service

import com.canceledsystems.tmdbimages.config.S3Properties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository
import software.amazon.awssdk.awscore.exception.AwsServiceException
import software.amazon.awssdk.core.exception.SdkClientException
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest

@Repository
class S3ImageRepository(private val client: S3Client,
                        private val properties: S3Properties): ImageRepository {

    private val log: Logger = LoggerFactory.getLogger(javaClass)

    override fun get(path: String): Image? {
        try {
            val request = GetObjectRequest.builder()
                .key(path)
                .bucket(properties.bucket)
                .build()
            return client.getObject(request).use {
                val response = it.response()
                Image(response.contentType(), it.readBytes())
            }
        } catch (e: AwsServiceException) {
            if (e.statusCode() == 404) {
                return null
            } else {
                throw S3Exception("Failed to retrieve $path", e)
            }
        } catch (e: SdkClientException) {
            throw S3Exception("Failed to retrieve $path", e)
        }
    }

    override fun save(path: String, image: Image) {
        try {
            val request = PutObjectRequest.builder()
                .contentType(image.mimeType)
                .contentLength(image.data.size.toLong())
                .bucket(properties.bucket)
                .key(path)
                .build()
            val requestBody = RequestBody.fromBytes(image.data)
            client.putObject(request, requestBody)
        } catch (e: AwsServiceException) {
            log.info("Failed to upload $path (status: ${e.statusCode()}, error: ${e.awsErrorDetails()?.errorMessage()})")
        } catch (e: SdkClientException) {
            throw S3Exception("Failed to upload $path", e)
        }
    }
}