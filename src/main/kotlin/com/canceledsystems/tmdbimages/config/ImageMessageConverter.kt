package com.canceledsystems.tmdbimages.config

import com.canceledsystems.tmdbimages.service.Image
import org.springframework.http.HttpInputMessage
import org.springframework.http.HttpOutputMessage
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.http.converter.HttpMessageNotWritableException
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class ImageMessageConverter : HttpMessageConverter<Image> {
    override fun canRead(arg0: Class<*>, mediaType: MediaType?): Boolean {
        val correctClass = arg0 == Image::class.java
        val correctType = supportedMediaTypes.contains(mediaType)
        return correctClass && correctType
    }

    override fun canWrite(arg0: Class<*>, arg1: MediaType?): Boolean {
        return false
    }

    override fun getSupportedMediaTypes(): List<MediaType> {
        return listOf(MediaType.IMAGE_JPEG, MediaType.IMAGE_GIF, MediaType.IMAGE_PNG)
    }

    @Throws(IOException::class, HttpMessageNotReadableException::class)
    override fun read(clazz: Class<out Image>, input: HttpInputMessage): Image {
        val data = input.body.readBytes()
        val contentType = input.headers.contentType
        return Image(contentType!!.type + "/" + contentType.subtype, data)
    }

    @Throws(HttpMessageNotWritableException::class)
    override fun write(image: Image, mediaType: MediaType?, outputMessage: HttpOutputMessage) {
        throw UnsupportedOperationException("not implemented")

    }
}