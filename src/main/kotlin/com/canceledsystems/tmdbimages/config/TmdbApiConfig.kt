package com.canceledsystems.tmdbimages.config

import com.github.xabgesagtx.tmdb.TmdbApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TmdbApiConfig {

    @Bean
    fun tmdbApi(properties: TmdbApiProperties): TmdbApi {
        return TmdbApi(properties.apiKey)
    }

}