package com.canceledsystems.tmdbimages.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "tmdb")
class TmdbApiProperties {

    lateinit var apiKey: String
}