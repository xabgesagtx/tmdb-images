package com.canceledsystems.tmdbimages.config

import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.Ticker
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.CachingConfigurer
import org.springframework.cache.caffeine.CaffeineCache
import org.springframework.cache.support.SimpleCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.TimeUnit

@Configuration
class CacheConfig: CachingConfigurer {

    @Bean
    override fun cacheManager(): CacheManager? {
        val cacheManager = SimpleCacheManager()
        val tmdbConfiguration = buildTmdbConfigurationCache()
        cacheManager.setCaches(listOf(tmdbConfiguration))
        return cacheManager
    }

    private fun buildTmdbConfigurationCache(): CaffeineCache {
        return CaffeineCache("tmdbConfiguration", Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .maximumSize(100)
                .ticker(ticker())
                .build())
    }

    @Bean
    fun ticker(): Ticker {
        return Ticker.systemTicker()
    }
}