# tmdb-images

An image proxy for themoviedb.org (tmdb)

It uses:
* spring boot for application bootstrapping
* [java tmdb api](https://github.com/Omertron/api-themoviedb) for connecting to tmdb
* S3 for caching images
* amazon SDK to connect to S3

## Configuration

Following java properties need to be provided to run the application either in development or production:

| Property | Description |
| -------- | ----------- |
| s3.bucket | Name of the S3 bucket |
| s3.region | Region where the S3 bucket is in |
| s3.endpoint | Endpoint to use for S3 connection (e.g. of amazon, digital ocena etc) |
| s3.key | ID of the S3 key |
| s3.secret | Secret of the S3 key |
| tmdb.apiKey | [API key](https://www.themoviedb.org/faq/api) from themoviedb.org |

There are multiple ways to provide the variables. Popular forms are:
* java properties (-D...)
* environment variables
* application.yml

More info on providing configuration can be found in the [spring boot documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config)


## Build

### Dependencies
This project comes with a gradle wrapper. Therefore you only need java to run it.

### Command
```
./gradlew build
```

## Run

### Dependencies
* java 21
* S3 compliant store (credentials)
* tmdb (API key)

### Command
```
java -jar build/libs/tmdb-images
```

The application serves the HTTP endpoints on port 8080 by default.

## Requesting an image

```
curl 'https://localhost:8080/image?path=/t17TmixmWhJeIC4ytLfis6frLYf.jpg&type=POSTER&size=MEDIUM'
```

There are the following parameters

| Parameter | Description |
| --------- | ----------- |
| path | The path of the image as provided by the movie or tv show JSON you got from the tmdb API |
| type | type of the image (POSTER, BACKDROP, STILL, PROFILE, LOGO) |
| size | As available size configurations can only be determined at runtime, This project offers a set of abstract size values (EXTRASMALL, SMALL, MEDIUM, LARGE, ANY) |

## Swagger UI

A UI for documenting the request parameters and trying them out will be available at [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) after starting the project. 