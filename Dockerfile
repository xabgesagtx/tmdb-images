ARG BUILD_HOME=/build-home

## build container
FROM gradle:8.11.1-jdk21-alpine as build-image

ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
WORKDIR $APP_HOME

COPY --chown=gradle:gradle build.gradle.kts settings.gradle.kts $APP_HOME/
COPY --chown=gradle:gradle src $APP_HOME/src

RUN gradle --no-daemon build


## run container
FROM eclipse-temurin:21-alpine

ARG BUILD_HOME
ENV APP_HOME=$BUILD_HOME
ENV JAVA_OPTS=""
COPY --from=build-image $APP_HOME/build/libs/tmdb-images.jar app.jar

ENTRYPOINT java $JAVA_OPTS -jar app.jar